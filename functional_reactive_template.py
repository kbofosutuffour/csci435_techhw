import reactivex as rx
import reactivex.operators as op

names = rx.of("Albert", "Edna", "Gertrude", "Hannah", "Elle", "Anna", "Booty", "Samantha", "Cornelia", "Sherman", "Winston")
prohibited_names = {"Booty", "Bot"}

def nonFunctional_isPalindrome(s):

    if len(s) <= 1:
        return True
    
    s = s.lower()

    l_index = 0
    r_index = len(s) - 1

    while l_index < r_index:

        print(s[l_index] + " " + s[r_index])

        if s[l_index] != s[r_index]:
            return False
        else:
            l_index += 1
            r_index -= 1

    return True


def functional_isPalindrome(s):

    #Base Case
    if len(s) <= 1:
        return True

    #Convert the string to all lowercase
    new_s = s.lower()
    
    if (new_s[0] == new_s[-1]):
        return functional_isPalindrome(new_s[1:len(s)-1])
    
    else:
        return False

def isPalindrome(n, foo = functional_isPalindrome):
    return foo(n)

player_stream = names.pipe(
    op.filter(lambda n : (n not in prohibited_names) and (not isPalindrome(n)))
)
# After filtering, player_stream should have 6 items


#Assigning Teams
red_team = set()
blue_team = set()

total_players = 0

def add_player(player, team):
    global total_players 
    total_players += 1
    team.add(player)

player_stream.subscribe(
    lambda name : add_player(name, red_team) if (not total_players % 2) else add_player(name, blue_team)
)

if (total_players % 2):
    add_player("Bot", blue_team)
    player_stream = rx.concat(player_stream, rx.of("Bot"))

player_stream.subscribe(
    lambda x : print(x)
)

print(red_team)
print(blue_team)

#Playing the Game
from random import randint

r1_point_stream = rx.from_iterable([randint(0,20) for x in range(total_players)])

scores = rx.zip(player_stream, r1_point_stream)   # Creates a stream of tuples in the form (player_name, score)

point_totals = [0,0]    # [red team score, blue team score]

def update_points(score_tuple):
    if score_tuple[0] in red_team:
        point_totals[0] += score_tuple[1]
    else:
        point_totals[1] += score_tuple[1]

scores.subscribe(
    lambda score_tuple : update_points(score_tuple)
)

print(point_totals)

#Final Round

# Create a new stream of point values

final_point_stream = rx.from_iterable([randint(0,20) for x in range(total_players)])

losing_team = red_team
#Set the condition for blue_team being the losing team
if point_totals[0] > point_totals[1]:
    losing_team = blue_team

final_point_stream.pipe(
    op.map(lambda x : x * 2 if (x % 2 == 0) else x),
    op.map(lambda x : x // 3 if (x % 3 == 0) else x)
)

f_scores = rx.zip(player_stream, final_point_stream)

def final_update_points(score_tuple):
    #if the player is on the losing team, multiply their score by 1.1
    if {score_tuple[0] == losing_team}:
        score_tuple[1] = score_tuple[1] * 1.1
    update_points(score_tuple)

def print_final_results():
    if point_totals[0] > point_totals[1]:
        print(f"The winner is the red team with a score of {point_totals[0]} to {point_totals[1]}")
    else:
        print(f"The winner is the blue team with a score of {point_totals[1]} to {point_totals[0]}")

f_scores.subscribe(
    on_next= lambda score_tuple : update_points(score_tuple),
    on_completed= lambda : print(point_totals)
)

